$(document).ready(function () {
    $('.add-btn').on('click', function (e) {
        e.preventDefault();
        var oldInput = $(this).parent().find('input').first();
        var newInput = oldInput.clone().attr('value', '');
        newInput.insertAfter($(this));
        $('<button class="btn-icon btn-icon-only btn-pill btn btn-danger margin-20 remove-btn"><i class="pe-7s-less btn-icon-wrapper"> </i></button>').insertAfter(newInput);
        $('.cloned-input-wrapper input, .cloned-input-wrapper .add-btn').addClass('margin-20');
    });

    $('body').on('click', '.remove-btn', function (e) {
        e.preventDefault();
        $(this).prev('.cloned-input').remove();
        $(this).remove();
    });
});
