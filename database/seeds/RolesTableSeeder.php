<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role as ModelsRole;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Admin',
            'Societe',
            'User',
        ];

        foreach ($data as $d) {
            ModelsRole::firstOrCreate(['name' => $d]);
        }
    }
}
