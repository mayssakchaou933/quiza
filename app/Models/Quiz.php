<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Quiz extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'win_points',
        'quiz_category_id',
        'user_id',
    ];

    public function category() {
        return $this->belongsTo(QuizCategory::class, 'quiz_category_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
}
