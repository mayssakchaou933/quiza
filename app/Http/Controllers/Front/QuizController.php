<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Quiz;

class QuizController extends Controller
{
    /**
     * @return  Response
     */
    public function index()
    {
        $quizzes = Quiz::get();

        return view('front.quizzes.index', compact('quizzes'));
    }

    public function show($id)
    {
        $quiz = Quiz::with(['category', 'user'])->findOrFail($id);

        return view('front.quizzes.show', compact('quiz'));
    }
}
