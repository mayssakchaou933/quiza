<?php

namespace App\Http\Controllers\Back;

use Spatie\Permission\Models\Role as ModelsRole;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\User;

class UserController extends Controller
{
    /**
     * @return  Response
     */
    public function index()
    {
        $users = User::with('roles')->get();

        return view('back.users.index', compact('users'));
    }

    /**
     * @return  Response
     */
    public function create()
    {
        $roles = ModelsRole::get();

        return view('back.users.create', compact('roles'));
    }

    /**
     * @param    \App\Http\Requests\UserRequest $request
     * @return  Response
     */
    public function store(UserRequest $request)
    {
        $data = $request->all();

        $data['password'] = bcrypt($data['password']);

        $user = User::create($data);

        $user->assignRole(ModelsRole::find($request->role_id));

        return redirect()->route('back.users.index')
            ->with('success', trans('og.alert.success'));
    }

    public function show($id)
    {
        $user = User::with('roles')->findOrFail($id);

        return view('back.users.show', compact('user'));
    }

    public function edit($id)
    {
        $user = User::with('roles')->findOrFail($id);
        $roles = ModelsRole::get();

        return view('back.users.edit', compact('user', 'roles'));
    }

    /**
     * @param    \App\Http\Requests\UserRequest $request
     * @param    int $id
     * @return  Response
     */
    public function update(UserRequest $request, $id)
    {
        $user = User::findOrFail($id);

        $data = $request->all();

        if ($data['password']) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        $user->update($data);

        $user->syncRoles([ModelsRole::find($request->role_id)]);

        return redirect()->route('back.users.index')
            ->with('success', trans('og.alert.success'));
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('back.users.index')->with('success', trans('og.alert.success'));
    }
}
