<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Quiz;
use App\Models\QuizCategory;
use App\Http\Requests\QuizRequest;

class QuizController extends Controller
{
    /**
     * @return  Response
     */
    public function index()
    {
        $quizzes = Quiz::get();

        return view('back.quizzes.index', compact('quizzes'));
    }

    /**
     * @return  Response
     */
    public function create()
    {
        $quiz_categories = QuizCategory::get();

        return view('back.quizzes.create', compact('quiz_categories'));
    }

    /**
     * @param    \App\Http\Requests\QuizRequest $request
     * @return  Response
     */
    public function store(QuizRequest $request)
    {
        $data = $request->all();
        $data['user_id'] = auth()->user()->id;

        $quiz = Quiz::create($data);

        return redirect()->route('back.quizzes.show', $quiz->id)
            ->with('success', trans('og.alert.success'));
    }

    public function show($id)
    {
        $quiz = Quiz::with(['category', 'user'])->findOrFail($id);

        return view('back.quizzes.show', compact('quiz'));
    }

    public function edit($id)
    {
        $quiz = Quiz::with(['category', 'user'])->findOrFail($id);
        $quiz_categories = QuizCategory::get();

        return view('back.quizzes.edit', compact('quiz', 'quiz_categories'));
    }

    /**
     * @param    \App\Http\Requests\QuizRequest $request
     * @param    int $id
     * @return  Response
     */
    public function update(QuizRequest $request, $id)
    {
        $quiz = Quiz::findOrFail($id);

        $quiz->update($request->all());

        return redirect()->route('back.quizzes.index')
            ->with('success', trans('og.alert.success'));
    }

    public function destroy($id)
    {
        $quiz = Quiz::findOrFail($id);
        $quiz->delete();

        return redirect()->route('back.quizzes.index')->with('success', trans('og.alert.success'));
    }
}
