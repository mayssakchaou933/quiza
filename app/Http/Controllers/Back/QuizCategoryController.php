<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Http\Requests\QuizCategoryRequest;
use App\Models\QuizCategory;

class QuizCategoryController extends Controller
{
    /**
     * @return  Response
     */
    public function index()
    {
        $quiz_categories = QuizCategory::get();

        return view('back.quiz_categories.index', compact('quiz_categories'));
    }

    /**
     * @return  Response
     */
    public function create()
    {
        return view('back.quiz_categories.create');
    }

    /**
     * @param    \App\Http\Requests\QuizCategoryRequest $request
     * @return  Response
     */
    public function store(QuizCategoryRequest $request)
    {
        $quiz_category = QuizCategory::create($request->all());

        return redirect()->route('back.quiz_categories.show', $quiz_category->id)
            ->with('success', trans('og.alert.success'));
    }

    public function show($id)
    {
        $quiz_category = QuizCategory::findOrFail($id);

        return view('back.quiz_categories.show', compact('quiz_category'));
    }

    public function edit($id)
    {
        $quiz_category = QuizCategory::findOrFail($id);

        return view('back.quiz_categories.edit', compact('quiz_category'));
    }

    /**
     * @param    \App\Http\Requests\QuizCategoryRequest $request
     * @param    int $id
     * @return  Response
     */
    public function update(QuizCategoryRequest $request, $id)
    {
        $quiz_category = QuizCategory::findOrFail($id);

        $quiz_category->update($request->all());

        return redirect()->route('back.quiz_categories.index')
            ->with('success', trans('og.alert.success'));
    }

    public function destroy($id)
    {
        $quiz_category = QuizCategory::findOrFail($id);
        $quiz_category->delete();

        return redirect()->route('back.quiz_categories.index')->with('success', trans('og.alert.success'));
    }
}
