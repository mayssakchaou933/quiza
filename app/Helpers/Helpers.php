<?php

if (!function_exists('getActions')) {
    function getActions($key, $module, $permissionName = null, $type = 'index')
    {
        $buttons = '';
        $previewBtn = false;

        if ($type == 'index') {
            $preview = '<a href="'. route("back.$module.show", $key) .'" class="mb-2 mr-2 btn-pill btn-transition btn btn-outline-info">
                <i class="pe-7s-look btn-icon-wrapper"> </i>
                Preview
            </a>';

            $previewBtn = true;
        }

        $edit = '<a href="'. route("back.$module.edit", $key).'"
            class="mb-2 mr-2 btn-pill btn-transition btn btn-outline-warning">
            <i class="pe-7s-pen btn-icon-wrapper"> </i>
            Update
        </a>';

        $delete = '<form style="display:inline" action="'. route("back.$module.destroy", $key) .'" method="POST">
            <input type="hidden" name="_method" value="DELETE">'. csrf_field() .'
            <span data-placement="top" data-toggle="tooltip" title="Delete">
                <button class="mb-2 mr-2 btn-pill btn-transition btn btn-outline-danger" type="submit"
                    onclick="return confirm('.'Are you sure?'.')">
                    <i class="pe-7s-trash btn-icon-wrapper"> </i>
                    Delete
                </button>
            </span>
        </form>';

        if ($previewBtn) {
            $buttons .= $preview;
        }
        $buttons .= $edit.$delete;

        return $buttons;
    }
}
