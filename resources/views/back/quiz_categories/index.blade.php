@extends('back.layouts.templates.default')

@section('icon')albums @endsection

@section('breadcrumbs')
Quiz Categories
@endsection

@section('buttons')
<a class="mb-2 mr-2 btn-pill btn-hover-shine btn btn-info" href="{{ route('back.quiz_categories.create') }}">
    Add Quiz Categories
</a>
@endsection

@section('main-content')
<h5 class="card-title">List</h5>
<table style="width: 100%;" id="example" class="table table-hover table-striped table-bordered">
    <thead>
        <tr>
            <th>Name</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($quiz_categories as $quiz_category)
        <tr>
            <td>{{ $quiz_category->name }}</td>
            <td>
                {!! getActions($quiz_category->id, 'quiz_categories', 'quiz_categories') !!}
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Name</th>
            <th>Actions</th>
        </tr>
    </tfoot>
</table>
@endsection
