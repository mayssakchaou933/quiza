@extends('back.layouts.templates.default')

@section('icon')albums @endsection

@section('breadcrumbs')
<a href="{{ route('back.quiz_categories.index') }}">
    quiz_categories
</a>
> Détail
@endsection

@section('buttons')
<a class="mb-2 mr-2 btn-pill btn-hover-shine btn btn-info" href="{{ route('back.quiz_categories.create') }}">
    Add quiz_category
</a>
@endsection

@section('main-content')
<h5 class="card-title">Détail</h5>
<table class="mb-0 table">
    <tbody>
        <tr>
            <td>ID</td>
            <td>
                {{ $quiz_category->id }}
            </td>
        </tr>
        <tr>
            <td>Name</td>
            <td>{{ $quiz_category->name }}</td>
        </tr>
        <tr>
            <td>Description</td>
            <td>{{ $quiz_category->description }}</td>
        </tr>
        <tr>
            <td>Actions</td>
            <td>
                {!! getActions($quiz_category->id, 'quiz_categories', 'quiz_categories', 'show') !!}
            </td>
        </tr>
    </tbody>
</table>
@endsection
