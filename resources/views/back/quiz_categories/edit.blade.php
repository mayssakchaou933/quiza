@extends('back.layouts.templates.default')

@section('icon')albums @endsection

@section('breadcrumbs')
<a href="{{ route('back.quiz_categories.index') }}">
    Quiz Categories
</a>
> Edit
@endsection

@section('main-content')
<h5 class="card-title">Edit Quiz Categories</h5>

@include('_common.alerts.messages')

<form class="" method="POST" action="{{ route('back.quiz_categories.update', $quiz_category->id) }}">
    @method('PUT')
    @csrf

    <div class="position-relative form-group">
        <label for="name" class="">Name *</label>
        <input type="text" class="form-control" name="name"
            value="{{ old('name',$quiz_category->name) }}" required autofocus>
    </div>

    <div class="position-relative form-group">
        <label for="description" class="">Description</label>
        <textarea name="description" class="form-control" cols="10" rows="5">{{ old('description',$quiz_category->description) }}</textarea>
    </div>

    <button class="mt-1 btn btn-primary">Submit</button>
</form>
@endsection
