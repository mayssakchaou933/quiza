<div class="app-header header-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                    data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>

    <div class="app-header__content">
        <div class="app-header-left">
            <ul class="header-megamenu nav">
                <li class="dropdown nav-item">
                    <a aria-haspopup="true" data-toggle="dropdown" class="nav-link" aria-expanded="false">
                        <i class="nav-link-icon pe-7s-settings"></i>
                        Admin
                        <i class="fa fa-angle-down ml-2 opacity-5"></i>
                    </a>
                    <div tabindex="-1" role="menu" aria-hidden="true"
                        class="dropdown-menu-rounded dropdown-menu-lg rm-pointers dropdown-menu">

                        <div class="dropdown-menu-header">
                            <div class="dropdown-menu-header-inner bg-success">
                                <div class="menu-header-image opacity-1"
                                style="background-image: url({{ asset('back/assets/images/dropdown-header/abstract3.jpg') }});">
                                </div>
                                <div class="menu-header-content text-left">
                                    <h5 class="menu-header-title">Change your shop</h5>
                                </div>
                            </div>
                        </div>

                        <form action="" method="post">
                            <button type="submit" class="dropdown-item">
                                <i class="dropdown-icon lnr-file-empty"> </i>
                                Item 1
                            </button>
                        </form>

                    </div>
                </li>
            </ul>
        </div>

        <div class="app-header-right">
            <div class="header-btn-lg pr-0">
                <div class="widget-content p-0">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="btn-group">
                                <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                    <img width="42" class="rounded-circle"
                                        src="{{ asset("/back/assets/images/avatars/1.png") }}" alt="">
                                    <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                </a>
                                <div tabindex="-1" role="menu" aria-hidden="true"
                                    class="rm-pointers dropdown-menu-lg dropdown-menu dropdown-menu-right">
                                    <div class="dropdown-menu-header">
                                        <div class="dropdown-menu-header-inner bg-info">
                                            <div class="menu-header-image opacity-2"
                                            style="background-image: url('{{ asset('back/assets/images/dropdown-header/city3.jpg') }}');">
                                            </div>
                                            <div class="menu-header-content text-left">
                                                <div class="widget-content p-0">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left mr-3">
                                                            <img width="42" class="rounded-circle"
                                                                src="{{ asset("/back/assets/images/avatars/1.png") }}"
                                                                alt="">
                                                        </div>
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">
                                                                Admin
                                                            </div>
                                                        </div>
                                                        <div class="widget-content-right mr-2">
                                                            <form action="route_logout" method="post">
                                                                @csrf
                                                                <button type="submit" class="btn-pill btn-shadow btn-shine btn btn-focus">
                                                                    Logout
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="nav flex-column">
                                        <li class="nav-item-btn text-center nav-item">
                                            <a href="change_password" class="btn-wide btn btn-primary btn-sm">
                                                Changer mot de passe
                                            </a>
                                        </li>
                                        <li class="nav-item-btn text-center nav-item">
                                            <a href="change-code" class="btn-wide btn btn-primary btn-sm">
                                                Changer code
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="widget-content-left  ml-3 header-user-info">
                            <div class="widget-heading">
                                Nom
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
