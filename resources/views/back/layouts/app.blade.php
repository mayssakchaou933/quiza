<!doctype html>
<html lang="en">

<head>
    @include('back.layouts.partials.head')

    @include('back.layouts.partials.css')
</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-header fixed-sidebar">
        @include('back.layouts.partials.top_navigation')

        {{-- @include('back.layouts.partials.configuration') --}}

        <div class="app-main">
            @include('back.layouts.partials.sidebar')

            @yield('content')
        </div>
    </div>

    @include('back.layouts.partials.right_navigation')

    <div class="app-drawer-overlay d-none animated fadeIn"></div>

    <div class="loader_upload" style="display: none">
        <div class="line-scale-pulse-out">
            <div class="bg-warning"></div>
            <div class="bg-warning"></div>
            <div class="bg-warning"></div>
            <div class="bg-warning"></div>
            <div class="bg-warning"></div>
        </div>
    </div>
    @include('back.layouts.partials.js')
</body>

</html>
