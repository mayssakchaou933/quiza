@extends('back.layouts.app')

@section('content')

<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-@yield('icon') icon-gradient bg-tempting-azure">
                        </i>
                    </div>
                    <div>
                        @yield('breadcrumbs')
                    </div>
                </div>
                <div class="page-title-actions">
                    @yield('buttons')
                </div>
            </div>
        </div>
        <div class="main-card mb-3 card">
            <div class="card-body">
                @yield('main-content')
            </div>
        </div>
    </div>
</div>
@endsection
