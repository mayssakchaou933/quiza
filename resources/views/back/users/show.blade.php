@extends('back.layouts.templates.default')

@section('icon')user @endsection

@section('breadcrumbs')
<a href="{{ route('back.users.index') }}">
    Users
</a>
> Détail
@endsection

@section('buttons')
<a class="mb-2 mr-2 btn-pill btn-hover-shine btn btn-info" href="{{ route('back.users.create') }}">
    Add user
</a>
@endsection

@section('main-content')
<h5 class="card-title">Détail</h5>
<table class="mb-0 table">
    <tbody>
        <tr>
            <td>ID</td>
            <td>
                {{ $user->id }}
            </td>
        </tr>
        <tr>
            <td>Name</td>
            <td>{{ $user->name }}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>{{ $user->email??null }}</td>
        </tr>
        <tr>
            <td>Role</td>
            <td>
                @if(! ($user->roles)->isEmpty())
                    {{ $user->roles->first()->name }}
                @endif
            </td>
        </tr>
        <tr>
            <td>Actions</td>
            <td>
                {!! getActions($user->id, 'users', 'Users', 'show') !!}
            </td>
        </tr>
    </tbody>
</table>
@endsection
