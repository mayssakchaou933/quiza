@extends('back.layouts.templates.default')

@section('icon')user @endsection

@section('breadcrumbs')
Users
@endsection

@section('buttons')
<a class="mb-2 mr-2 btn-pill btn-hover-shine btn btn-info" href="{{ route('back.users.create') }}">
    Add user
</a>
@endsection

@section('main-content')
<h5 class="card-title">List</h5>
<table style="width: 100%;" id="example" class="table table-hover table-striped table-bordered">
    <thead>
        <tr>
            <th>Name</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{ $user->name }}</td>
            <td>
                {!! getActions($user->id, 'users', 'Users') !!}
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Name</th>
            <th>Actions</th>
        </tr>
    </tfoot>
</table>
@endsection
