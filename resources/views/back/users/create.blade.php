@extends('back.layouts.templates.default')

@section('icon')user @endsection

@section('breadcrumbs')
<a href="{{ route('back.users.index') }}">
    Users
</a>
> Create
@endsection

@section('main-content')
<h5 class="card-title">Create user</h5>

@include('_common.alerts.messages')

<form class="" method="POST" action="{{ route('back.users.store') }}">
    @csrf

    <div class="position-relative form-group">
        <label for="name" class="">Name *</label>
        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
    </div>

    <div class="position-relative form-group">
        <label for="email" class="">Email *</label>
        <input type="email" class="form-control" name="email" value="{{ old('email') }}" required>
    </div>

    <div class="position-relative form-group">
        <label for="password" class="">Password *</label>
        <input type="password" class="form-control" name="password" required>
    </div>

    <div class="position-relative form-group">
        <label for="role_id">Role *</label>
        <select class="mb-2 form-control" name="role_id" required>
            @foreach($roles as $role)
                <option value="{{ $role->id }}" @if(old('role_id') == $role->id) selected @endif >{{ $role->name }}</option>
            @endforeach
        </select>
    </div>

    <button class="mt-1 btn btn-primary">Submit</button>
</form>
@endsection
