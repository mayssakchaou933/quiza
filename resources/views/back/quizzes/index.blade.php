@extends('back.layouts.templates.default')

@section('icon')helm @endsection

@section('breadcrumbs')
Quizzes
@endsection

@section('buttons')
<a class="mb-2 mr-2 btn-pill btn-hover-shine btn btn-info" href="{{ route('back.quizzes.create') }}">
    Add Quizzes
</a>
@endsection

@section('main-content')
<h5 class="card-title">List</h5>
<table style="width: 100%;" id="example" class="table table-hover table-striped table-bordered">
    <thead>
        <tr>
            <th>Name</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($quizzes as $quiz)
        <tr>
            <td>{{ $quiz->name }}</td>
            <td>
                {!! getActions($quiz->id, 'quizzes', 'quizzes') !!}
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Name</th>
            <th>Actions</th>
        </tr>
    </tfoot>
</table>
@endsection
