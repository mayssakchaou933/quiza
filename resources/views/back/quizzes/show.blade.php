@extends('back.layouts.templates.default')

@section('icon')helm @endsection

@section('breadcrumbs')
<a href="{{ route('back.quizzes.index') }}">
    quizzes
</a>
> Détail
@endsection

@section('buttons')
<a class="mb-2 mr-2 btn-pill btn-hover-shine btn btn-info" href="{{ route('back.quizzes.create') }}">
    Add quiz
</a>
@endsection

@section('main-content')
<h5 class="card-title">Détail</h5>
<table class="mb-0 table">
    <tbody>
        <tr>
            <td>ID</td>
            <td>
                {{ $quiz->id }}
            </td>
        </tr>
        <tr>
            <td>Name</td>
            <td>{{ $quiz->name }}</td>
        </tr>
        <tr>
            <td>Win points</td>
            <td>{{ $quiz->win_points }}</td>
        </tr>
        <tr>
            <td>Description</td>
            <td>{{ $quiz->description }}</td>
        </tr>
        <tr>
            <td>Category</td>
            <td>{{ optional($quiz->category)->name }}</td>
        </tr>
        <tr>
            <td>User</td>
            <td>{{ optional($quiz->user)->name }}</td>
        </tr>
        <tr>
            <td>Actions</td>
            <td>
                {!! getActions($quiz->id, 'quizzes', 'quizzes', 'show') !!}
            </td>
        </tr>
    </tbody>
</table>
@endsection
