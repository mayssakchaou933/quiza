@extends('back.layouts.templates.default')

@section('icon')helm @endsection

@section('breadcrumbs')
<a href="{{ route('back.quizzes.index') }}">
    Quizzes
</a>
> Create
@endsection

@section('main-content')
<h5 class="card-title">Create Quizzes</h5>

@include('_common.alerts.messages')

<form class="" method="POST" action="{{ route('back.quizzes.store') }}">
    @csrf

    <div class="position-relative form-group">
        <label for="name" class="">Name *</label>
        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
    </div>

    <div class="position-relative form-group">
        <label for="win_points" class="">Win points *</label>
        <input type="number" class="form-control" name="win_points" value="{{ old('win_points') }}" required>
    </div>

    <div class="position-relative form-group">
        <label for="description" class="">Description</label>
        <textarea name="description" class="form-control" cols="10" rows="5">{{ old('description') }}</textarea>
    </div>

    <div class="position-relative form-group">
        <label for="quiz_category_id">Category *</label>
        <select class="mb-2 form-control" name="quiz_category_id" required>
            @foreach($quiz_categories as $quiz_category)
                <option value="{{ $quiz_category->id }}" @if(old('quiz_category_id') == $quiz_category->id) selected @endif >{{ $quiz_category->name }}</option>
            @endforeach
        </select>
    </div>

    <button class="mt-1 btn btn-primary">Submit</button>
</form>
@endsection
