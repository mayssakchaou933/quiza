@extends('front.layouts.app')

@section('content')

    <div class="multisteps-form__form" id="wizard">
        <div class="form-area position-relative">
            <div class="multisteps-form__panel js-active" data-animation="slideHorz">
                <div class="wizard-forms">
                    <div class="inner pb-100 clearfix">
                        <div class="form-content pera-content">
                            <div class="step-inner-content">
                                <span class="step-no">
                                    @guest
                                        <a class="dropdown-item" href="{{ route('register') }}">
                                            Register
                                        </a>
                                        <a class="dropdown-item" href="{{ route('login') }}">
                                            Login
                                        </a>
                                    @else
                                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                            document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                            style="display: none;">
                                            @csrf
                                        </form>
                                    @endguest
                                </span>
                                <h2>Choose Your quiz</h2>
                                <div class="step-box">
                                    <div class="col-md-12">
                                        <div class="form-inner-area" style="margin-top:0px; margin-bottom:25px">
                                            <div class="plan-area" style="padding-top: 0px">
                                                <div class="text-center active" style="width: 100%">
                                                    <div class="plan-icon">
                                                        <i class="fas fa-chess-queen"></i>
                                                    </div>
                                                    <div class="plan-text">
                                                        <a href="{{ route('front.quizzes.show', $quiz->id) }}">
                                                            <h3>{{ $quiz->name }}</h3>
                                                        </a>
                                                        <p>{{ $quiz->description }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
