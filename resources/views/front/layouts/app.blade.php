<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Quiz</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    @include('front.layouts.partials.css')
</head>

<body>
	<div class="wrapper">
		<div class="steps-area steps-area-fixed">
			<div class="image-holder">
				<img src="{{ asset("/front/img/side-img.jpg") }}" alt="">
			</div>
        </div>

        @yield('content')

	</div>
	@include('front.layouts.partials.js')
</body>
</html>
