<link rel="stylesheet" href="{{ asset("/front/css/bootstrap.min.css") }}">
<link rel="stylesheet" href="{{ asset("/front/css/bootstrap-datepicker.css") }}">
<link rel="stylesheet" href="{{ asset("/front/css/animate.min.css") }}">
<link rel="stylesheet" href="{{ asset("/front/css/fontawesome-all.css") }}">
<link rel="stylesheet" href="{{ asset("/front/css/style.css") }}">
<link rel="stylesheet" type="text/css" href="{{ asset("/front/css/colors/switch.css") }}">
