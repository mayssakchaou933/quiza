<div class="loader-wrapper d-flex justify-content-center align-items-center" style="width:100% !important">
    <div class="loader">
        <div class="ball-clip-rotate">
            <div style="background-color: #fff;border: 2px solid #3f6ad8"></div>
        </div>
    </div>
</div>
