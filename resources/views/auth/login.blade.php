@extends('front.layouts.app')

@section('content')
    <form class="multisteps-form__form" method="POST" action="{{ route('login') }}" id="wizard">
        @csrf
        <div class="form-area position-relative">
            <div class="multisteps-form__panel js-active" data-animation="slideHorz">
                <div class="wizard-forms">
                    <div class="inner pb-100 clearfix">
                        <div class="form-content pera-content">
                            <div class="step-inner-content">
                                <span class="step-no">Sign in</span>
                                <h2>Login to play quiz and test your knowledge.</h2>
                                <div class="step-box">
                                    <div class="col-md-12">
                                        <div class="form-inner-area">

                                            <input id="email" type="email"
                                                class="form-control required @error('email') is-invalid @enderror "
                                                name="email" value="{{ old('email') }}" required autocomplete="email"
                                                autofocus>

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                                                name="password" required autocomplete="current-password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="actions">
                        <ul>
                            <li>
                                <a href="{{ route('register') }}">
                                    <span class="js-btn-next" title="NEXT">Register</span>
                                </a>
                            </li>
                            <li><button type="submit" class="js-btn-next" title="NEXT">Login <i
                                        class="fa fa-arrow-right"></i></button></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </form>
@endsection
