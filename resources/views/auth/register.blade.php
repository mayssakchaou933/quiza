@extends('front.layouts.app')

@section('content')

    <form class="multisteps-form__form" action="{{ route('register') }}" id="wizard" method="POST">
        @csrf
        <div class="form-area position-relative">
            <!-- div 1 -->
            <div class="multisteps-form__panel js-active" data-animation="slideHorz">
                <div class="wizard-forms">
                    <div class="inner pb-100 clearfix">
                        <div class="form-content pera-content">
                            <div class="step-inner-content">
                                <span class="step-no">Sign up</span>
                                <h2>Sign up for free to play quiz and test your knowledge.</h2>
                                <div class="step-box">
                                    <div class="col-md-12">
                                        <div class="form-inner-area">
                                            <input id="name" type="text"
                                                class="form-control required @error('name') is-invalid @enderror"
                                                name="name" value="{{ old('name') }}" required autocomplete="name" autofocus
                                                placeholder="Name">
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                            <input id="email" type="email"
                                                class="form-control required @error('email') is-invalid @enderror"
                                                name="email" value="{{ old('email') }}" required autocomplete="email"
                                                placeholder="Email">

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                            <input id="password" type="password"
                                                class="form-control @error('password') is-invalid @enderror" name="password"
                                                required autocomplete="new-password" placeholder="Password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                            <input id="password-confirm" type="password" class="form-control"
                                                name="password_confirmation" required autocomplete="new-password"
                                                placeholder="Password confirmation">


                                            <div class="language-select">
                                                <select name="role_id"
                                                    class="form-control required @error('role_id') is-invalid @enderror">
                                                    @foreach($roles as $role)
                                                        @if($role->name != 'Admin')
                                                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                @error('role_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.inner -->
                    <div class="actions">
                        <ul>
                            <li>
                                <a href="{{ route('login') }}">
                                    <span class="js-btn-next" title="NEXT">Login</span>
                                </a>
                            </li>
                            <li><button type="submit" class="js-btn-next" title="Register">Register <i
                                        class="fa fa-arrow-right"></i></button></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </form>
@endsection
