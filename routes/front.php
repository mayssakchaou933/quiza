<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Front', 'as' => 'front.'], function() {
    Route::get('/', 'QuizController@index')->name('home');

    Route::get('/quizzes', 'QuizController@index')->name('quizzes.index');
    Route::get('/quizzes/{id}', 'QuizController@show')->name('quizzes.show');

    Route::group(['namespace' => 'Front', 'middleware' => ['web', 'auth']], function() {

    });
});
